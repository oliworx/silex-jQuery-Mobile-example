$(document).on('pagebeforeshow', '#index', function(){
    ajax.ajaxCall("handler.php", true);
});

$(document).on('click', '#user-list li', function(){
    ajax.ajaxCall("handler.php/" + $(this).data('listid'), false);
});

var ajax = {
    parseJSON:function(result){
        var obj = jQuery.parseJSON(result);
        $('#user-list').empty();
        $.each(obj, function(key,row) {
            $('#user-list').append('<li data-listid="' + key + '"><a href="" data-id="' + row.id + '"><img src="http://ia.media-imdb.com/images/M/'+row.image+'"/><h3>' + row.name + '</h3><p>' + row.description + '</p></a></li>');
        });
        $('#user-list').listview('refresh');
    },
    parseJSONDetails:function(result){
        var obj = jQuery.parseJSON(result);
        $('#personal-data').empty();
        $('#personal-data').append('<li><img src="http://ia.media-imdb.com/images/M/'+obj.image+'"></li>');
        $('#personal-data').append('<li>Name: '+obj.name+'</li>');
        $('#personal-data').append('<li>Title: '+obj.description+'</li>');
        $('#personal-data').listview().listview('refresh');
        $.mobile.changePage( "#second");
    },
    ajaxCall: function(url, initialize) {
        $.ajax({
            url: url,
            async: 'true',
            success: function(result) {
                if (initialize) {
                    ajax.parseJSON(result);
                } else {
                    ajax.parseJSONDetails(result);
                }
            },
            error: function(request, error) {
                alert('Network error has occurred, please try again!');
            }
        });
    }
};
