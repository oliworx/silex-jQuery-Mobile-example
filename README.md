# silex-jQuery-Mobile-example

This is just a litte test application for me to get in touch with the php micro framework SILEX.

The original code comes from **Gajotres** : http://www.gajotres.net/creating-a-basic-restful-api-with-silex/2/

1. install dependencies: ```composer install```
2. start webserver: ```php -S localhost:12345```
3. open *http://localhost:12345* in your webbrowser

References:

* origin: http://www.gajotres.net/creating-a-basic-restful-api-with-silex/2/
* SILEX micro framework: http://silex.sensiolabs.org/
* jQuery Mobile: http://jquerymobile.com/
* jQuery: https://jquery.com/

Build status:
* Circle CI: [![CircleCI](https://circleci.com/gh/oliworx/silex-jQuery-Mobile-example.svg?style=svg)](https://circleci.com/gh/oliworx/silex-jQuery-Mobile-example)
* Semaphore CI: [![Build Status](https://semaphoreci.com/api/v1/oliworx/silex-jquery-mobile-example/branches/master/badge.svg)](https://semaphoreci.com/oliworx/silex-jquery-mobile-example)
