<?php

use Silex\WebTestCase;
class YourTest extends WebTestCase
{
    public function createApplication()
    {
        return require __DIR__.'/../app.php';
    }

    /**
     * @small
     */
    public function testApiIndex()
    {
        $client = $this->createClient();

        $crawler = $client->request('GET', '/');
        $response = $client->getResponse();

        $this->assertTrue($response->isOk());
        $this->assertEquals('UTF-8', $response->getCharset());

        $data = json_decode($response->getContent());
        $this->assertTrue(is_object($data));

        // convert object to nested array
        $data = json_decode(json_encode($data), true);
        $this->assertArrayHasKey('00001', $data );
        $this->assertGreaterThan(1, count($data));
    }

    /**
     * @small
     */
    public function testApiExistingItem()
    {
        $client = $this->createClient();
        $crawler = $client->request('GET', '/00001');
        $response = $client->getResponse();
        $this->assertTrue($response->isOk());
        $this->assertEquals('UTF-8', $response->getCharset());

        $data = json_decode($response->getContent());
        $this->assertTrue(is_object($data));
        $this->assertEquals('Peter Jackson', $data->name);
        $this->assertObjectHasAttribute('description', $data);
        $this->assertObjectHasAttribute('image', $data);
    }

    /**
     * @small
     */
    public function testApiMissingItem()
    {
        $client = $this->createClient();
        $crawler = $client->request('GET', '/10000');
        $response = $client->getResponse();
        $this->assertTrue($response->isNotFound());
    }

}